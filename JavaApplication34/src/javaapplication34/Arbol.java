
package javaapplication34;

public class Arbol {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int dato;
        String nombre;
        ArbolBinario miArbol = new ArbolBinario();
        miArbol.agregarNodo(9, "DEMO");
        miArbol.agregarNodo(3, "DEMO");
        miArbol.agregarNodo(10, "DEMO");
        miArbol.agregarNodo(1, "DEMO");
        miArbol.agregarNodo(6, "DEMO");
        miArbol.agregarNodo(14, "DEMO");
        miArbol.agregarNodo(4, "DEMO");
        miArbol.agregarNodo(7, "DEMO");
        miArbol.agregarNodo(13, "DEMO");
        
        System.out.println("InOrden");
        if (!miArbol.estaVacio()){
            miArbol.inOrden(miArbol.raiz);
        }
        System.out.println("PreOrden");
        if (!miArbol.estaVacio()){
            miArbol.preOrden(miArbol.raiz);
        }
        System.out.println("PostOrden");
        if (!miArbol.estaVacio()){
            miArbol.postOrden(miArbol.raiz);
        }
    }
    
}
